﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WCFClient.Models;
using BmiSReference;

namespace WCFClient.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private BmiSReference.Service1Client client = new BmiSReference.Service1Client();
        
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        
        public IActionResult Index()
        {
            return View();

          
        }

        [HttpPost]
        public IActionResult Index(double waga, double wzrost)
        {
            ViewBag.waga = waga;
            ViewBag.wzrost = wzrost;
            ViewBag.bmi = client.BmiAsync(ViewBag.waga,ViewBag.wzrost).Result;
            return View();
        }

        
        
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
